DROP DATABASE IF EXISTS tool_tracker;
CREATE DATABASE tool_tracker
  CHARACTER SET utf8 COLLATE utf8_bin;

USE tool_tracker;

CREATE TABLE user
(
  id          INT PRIMARY KEY    NOT NULL AUTO_INCREMENT,
  first_name  VARCHAR(64)        NOT NULL,
  password    VARCHAR(300)       NOT NULL,
  last_name   VARCHAR(64)        NOT NULL,
  user_name   VARCHAR(64) UNIQUE NOT NULL,
  email       VARCHAR(64) UNIQUE NOT NULL,
  user_type   VARCHAR(64)        NOT NULL DEFAULT 'user',
  still_alive TINYINT DEFAULT 1  NOT NULL,
  notes       VARCHAR(512)
)
  ENGINE = INNODB;

CREATE TABLE garage
(
  id          INT PRIMARY KEY   NOT NULL AUTO_INCREMENT,
  garage_name VARCHAR(64)       NOT NULL,
  location    VARCHAR(300)      NOT NULL,
  created     DATE              NOT NULL,
  notes       VARCHAR(512),
  still_alive TINYINT DEFAULT 1 NOT NULL
)
  ENGINE INNODB;

CREATE TABLE tool
(
  id          INT PRIMARY KEY   NOT NULL AUTO_INCREMENT,
  tool_name   VARCHAR(64)       NOT NULL,
  category    VARCHAR(64)       NOT NULL,
  description VARCHAR(10000),
  notes       VARCHAR(512),
  still_alive TINYINT DEFAULT 1 NOT NULL,
  user_id     INT,
  garage_id   INT,
  FOREIGN KEY (user_id) REFERENCES user (id),
  FOREIGN KEY (garage_id) REFERENCES garage (id)
)
  ENGINE = INNODB AUTO_INCREMENT = 1000;

CREATE TABLE history
(
  id          INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  tool_id     INT             NOT NULL,
  user_id     INT,
  checked_out DATETIME,
  checked_in  DATETIME,
  FOREIGN KEY (tool_id) REFERENCES tool (id),
  FOREIGN KEY (user_id) REFERENCES user (id)
)
  ENGINE INNODB;

CREATE TABLE category
(
  category VARCHAR(64)   PRIMARY KEY NOT NULL
)
  ENGINE = INNODB;

CREATE TABLE setting
(
  id            INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  user_id       INT,
  setting_key   VARCHAR(64),
  setting_value VARCHAR(64),
  FOREIGN KEY (user_id) REFERENCES user (id)
)
  ENGINE INNODB;