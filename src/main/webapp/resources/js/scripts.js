/********************************************
 *      Onload listeners for buttons.       *
 ********************************************/

$(function () {
    $("#btn_plusTool")
        .click(function (e) {
            var addToolSection = $('#add-tool');
            addToolSection.toggleClass('closed');
            var requiredBubbles = $('.requiredErrorMessage');
            requiredBubbles.remove();
            $(this).toggleClass("btn-add");

            if ($(this).text() == "+ Tool") {
                $(this).text("Close");
            } else {
                $(this).text("+ Tool");
            }
        });

    $("#btn_plusUser")
        .click(function (e) {
            var addToolSection = $('#add-user');
            addToolSection.toggleClass('closed');
            var requiredBubbles = $('.requiredErrorMessage');
            requiredBubbles.remove();
            $(this).toggleClass("btn-add");

            if ($(this).text() == "+ User") {
                $(this).text("Close");
            } else {
                $(this).text("+ User");
            }
        });

    $("#btn_plusGarage")
        .click(function (e) {
            var addToolSection = $('#add-garage');
            addToolSection.toggleClass('closed');
            var requiredBubbles = $('.requiredErrorMessage');
            requiredBubbles.remove();
            $(this).toggleClass("btn-add");

            if ($(this).text() == "+ Garage") {
                $(this).text("Close");
            } else {
                $(this).text("+ Garage");
            }
        });

    $("#btn_changePassword")
        .click(function (e) {
            var addToolSection = $('#change-password');
            addToolSection.toggleClass('closed');
            var requiredBubbles = $('.requiredErrorMessage');
            requiredBubbles.remove();
            $(this).toggleClass("btn-add");

            if ($(this).text() == "Change Password") {
                $(this).text("Close");
            } else {
                $(this).text("Change Password");
            }
        });
});


/********************************************
 *   Primefaces Datatable filter trigger    *
 ********************************************/

function toolSearchTrigger(widgetVar) {
    // delay 500 ms
    delayFunction(function () {
        PF(widgetVar).filter();
    }, 500);
}

/**
 * Delays given function by specified time.
 */
var delayFunction = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();