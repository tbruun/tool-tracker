$(function () {
    var toggleBtn = $(".cmn-toggle-switch");
    var menu = $(".menu");
    var menuBtn = $(".menu-btn");


    /*
     * toggles the mobile menu and animates toggle-btn
     */
    toggleBtn.on( "click", function(e) {
        menu.toggleClass("mobile-menu");
        toggleBtn.toggleClass("active");
    });

    /*
     * restores all states when menu item is selected.
     * if we implement animations...
     */
    menuBtn.on( "click", function(e) {
    });

    $(window).resize(function () {
        var w = $(window).width();
        if (w < 600 && toggleBtn.hasClass("active")) {
            toggleBtn.toggleClass("active");
        } else if (w > 600){
            menu.removeClass("mobile-menu");
        }
    });
});