package com.awesomegroup.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Filter that handles authorization. <br>
 * Every incoming request to this webapp is handled by this filter first. <br>
 *
 * In this application its only a few cases where requests is handled without having a valid session.
 * Login page beeing one of them. <br>
 *
 * This filter will make sure every disallowed request from non-valid users is redirected to login.jsf
 */
@WebFilter(filterName = "authFilter", urlPatterns = {"*.xhtml", "*.jsf", "*.jsp"})
public class AuthFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {}

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {
            // check whether session variable is set
            HttpServletRequest req = (HttpServletRequest) request;
            HttpServletResponse res = (HttpServletResponse) response;
            HttpSession ses = req.getSession(false);

            String reqURI = req.getRequestURI();

            if (reqURI.contains("/settings.jsf") && (ses != null && ses.getAttribute("usertype").equals("user"))) {
                res.sendRedirect(req.getContextPath() + "/login.jsf");  // regular user. Redirect to login page
            }
            if (reqURI.contains("/login.jsf") || (ses != null && ses.getAttribute("username") != null) ||
                    reqURI.contains("javax.faces.resource")) {
                // if user is at login page, or he has a session with a username we can let the request trough
                chain.doFilter(request, response);

            } else if (reqURI.contains("/resetpassword.jsf")) {
                chain.doFilter(request, response);
            }
            else {
                //if not we need to send user to loginpage
                res.sendRedirect(req.getContextPath() + "/login.jsf");  // Anonymous user. Redirect to login page
            }
        } catch (Throwable t) {
            System.out.println(t.getMessage());
        }
    }

    @Override
    public void destroy() {}

}