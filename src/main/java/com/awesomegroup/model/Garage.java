package com.awesomegroup.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

/**
 * Entity for a Garage <p>
 *
 * A Garage is a place to store tools when they are not associated with a user. But even if a user have taken a tool,
 * it will still be associated with a Garage. This is to know which garage the tool "belongs" to. <br>
 *
 * Still, a Tool is free to go to whichever garage he pleases. The garage id is only for convenience when a tool is
 * associated with a user.
 */
@Entity
@Table(name = "garage")

    /* -------------- */
    /* Entity Queries */
    /* -------------- */

@NamedQueries({
        @NamedQuery(name = "Garage.findAll",
                query = "SELECT g FROM Garage g WHERE g.stillAlive = 1"),
        @NamedQuery(name = "Garage.findByUserName",
                query = "SELECT g FROM Garage g WHERE g.stillAlive = 1 AND g.garageName = :garageName"),
        @NamedQuery(name = "Garage.findGarageById",
                query = "SELECT g FROM Garage g WHERE g.stillAlive = 1 AND g.id = :id")
})
/**
 * Named entity graphs from JPA 2.1 to trigger eager fetching of various relations when needed at runtime.
 */
@NamedEntityGraphs({
        @NamedEntityGraph(
                name = "FetchGarageWithTool",
                attributeNodes = {
                        @NamedAttributeNode("tools")
                }
        ),
        @NamedEntityGraph(
                name = "FetchGarageWithToolAndUser",
                attributeNodes = {
                        @NamedAttributeNode(value = "tools", subgraph = "toolGraph")
                },
                subgraphs = {
                        @NamedSubgraph(
                                name = "toolGraph",
                                attributeNodes = {
                                        @NamedAttributeNode("historyList")
                                }
                        )
                }
        )})
public class Garage implements Serializable {

    /* ------------ */
    /* Local fields */
    /* ------------ */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotNull
    @Column(name = "garage_name")
    private String garageName;
    @NotNull
    private String location;
    @NotNull
    private Date created;
    private String notes;
    @Column(name = "still_alive")
    private int stillAlive = 1;

    /* --------------- */
    /* Relation fields */
    /* --------------- */

    @OneToMany(mappedBy = "garage")
    private List<Tool> tools;

    /* ----------------- */
    /* Getters / Setters */
    /* ----------------- */

    public Garage() {
    }

    public Garage(String garageName, String location, Date created) {
        this.garageName = garageName;
        this.location = location;
        this.created = created;
    }

    public Garage(String garageName, String location, Date created, String notes) {
        this.garageName = garageName;
        this.location = location;
        this.created = created;
        this.notes = notes;
    }

    public int getStillAlive() {
        return stillAlive;
    }

    public void setStillAlive(int stillAlive) {
        this.stillAlive = stillAlive;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGarageName() {
        return garageName;
    }

    public void setGarageName(String garageName) {
        this.garageName = garageName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public List<Tool> getTools() {
        return tools;
    }

    public void setTools(List<Tool> tools) {
        this.tools = tools;
    }

    /* ----------------- */
    /* Hashcode / Equals */
    /* ----------------- */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Garage garage = (Garage) o;
        return Objects.equals(id, garage.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
