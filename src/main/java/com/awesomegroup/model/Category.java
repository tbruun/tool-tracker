package com.awesomegroup.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Entity for a tool category <p>
 *
 * A tool category is from the lookup table, and is primarily needed to populate the drop-down list when creating
 * a new tool.<br>
 * This entity is just a convenience when entering category.
 *
 * It is not connected to the "category" field in the tool table witch is still just stored as a plain string.<br>
 * - User is free to choose whichever string he wants, but if user does not select a String that matches a category,
 * one will be added to the Category table.
 */
@Entity
@Table(name = "category")
@NamedQueries({
        @NamedQuery(name = "Category.findAll",
                query = "SELECT c FROM Category c"),
        @NamedQuery(name = "Category.findByName",
                query = "SELECT c FROM Category c WHERE c.category = :category")}
)

public class Category implements Serializable {

    @Id
    private String category;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
