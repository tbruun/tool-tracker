package com.awesomegroup.util;

/**
 * Enum storing Mail constants
 */
public enum Mails {


    /* --------------- */
    /* Constant fields */
    /* --------------- */

    RESET_MAIL_MESSAGE("Hello, here is a link for you my friend. Just press the link below to reset your password"),
    RESET_MAIL_SUBJECT("ToolHub.makasd.se - You requested a password reset");


    /* --------------- */
    /* Instance fields */
    /* --------------- */

    private String value;

    Mails(String value){
        this.value = value;
    }


    /* ----------------- */
    /* Setters & Getters */
    /* ----------------- */

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
