package com.awesomegroup.util;

import com.awesomegroup.model.Tool;
import com.awesomegroup.model.User;
import com.awesomegroup.service.HistoryService;
import com.awesomegroup.service.MailService;
import com.awesomegroup.service.SettingService;
import com.awesomegroup.service.UserService;

import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

import static com.awesomegroup.util.Settings.RESET_PASSWORD;


/**
 * Utility class handling all schedule tasks.
 *
 * Runs automatically on specified time and date.
 */
@Stateless
public class TaskScheduler {

    /* ------------------------ */
    /* Injected instance fields */
    /* ------------------------ */

    @Inject
    HistoryService historyService;
    @Inject
    UserService userService;
    @Inject
    SettingService settingService;
    @Inject
    MailService mailService;


    /**
     * Schedule for weekly mail reminders.
     *
     * Promises to remind admins when tools have been
     * inactive for a long period of time.
     */
    @Schedule(hour = "07", minute = "00", second = "00", dayOfWeek = "Mon")
    public void sendScheduledMail() {
        List<Tool> oldTools = historyService.findOldTools();

        List<User> mailRecipentents = userService.findAdmins();

        if (!oldTools.isEmpty()) {
            mailRecipentents.forEach(user -> {
                boolean userWantsMail = Boolean.valueOf(
                        settingService.getSettingForUser(Settings.RECIEVE_WEEKLY_MAIL, user).getValue()
                );
                if (userWantsMail) {
                    String recipent = user.getEmail();
                    String subject = "TOOL HUB: Inactive tools";
                    String message = mailService.generateOldToolMessage(oldTools);
                    mailService.sendMail(recipent, subject, message);
                }
            });
        }
    }


    /**
     * Schedule for daily removal of password-reset tokens.<p>
     *
     * Will remove all data from the database related to reset of passwords.<br>
     * Old reset password links will no longer be working.
     */
    @Schedule(hour = "03", minute = "00", second = "00")
    public void removeOldPasswordResets() {
        settingService.removeAllSettingsByKey(RESET_PASSWORD);
    }
}
