package com.awesomegroup.util;

/**
 * Enum storing Setting constants.
 *
 * These constants are bound to all types
 * of settings a user/admin can choose from
 */
public enum Settings {

    /* --------------- */
    /* Constant fields */
    /* --------------- */

    RECIEVE_WEEKLY_MAIL("recieve_weekly_mail"),
    RESET_PASSWORD("reset_password");

    /* --------------- */
    /* Instance fields */
    /* --------------- */

    private String value;

    Settings(String value){
        this.value = value;
    }
    
    /* ----------------- */
    /* Setters & Getters */
    /* ----------------- */

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
