package com.awesomegroup.beans;

import com.awesomegroup.model.User;
import com.awesomegroup.service.SettingService;
import com.awesomegroup.service.UserService;
import com.awesomegroup.util.MessageType;
import com.awesomegroup.util.PasswordHash;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;


/**
 * Handling all and everything associated with
 * a password reset. Either for user or admin.
 */
@Named
@ViewScoped
public class ResetPasswordBean implements Serializable {


    /* ------------------------ */
    /* Injected instance fields */
    /* ------------------------ */

    @Inject
    NavigationBean navigationBean;
    @Inject
    UserService userService;
    @Inject
    MessageBean messageBean;
    @Inject
    SettingService settingService;

    /* --------------- */
    /* Instance fields */
    /* --------------- */

    private String resetPasswordToken;
    private String userName;
    private String newPassword;
    private String repeatPassword;


    /**
     * Validator and message generator.
     *
     * Promises to give the user valid information
     * during password reset.
     *
     * If all validation passes this method proceed to
     * reset the users password.
     */
    public void changeToNewPassword() {

        int userId = settingService.findUserBySettingGuid(resetPasswordToken).getId();
        User user = userService.findByUserID(userId);

        if (!userName.equals(user.getUserName())) {
            messageBean.createNotificationMessage(MessageType.NOTIFICATION_ERROR, "Failed!", "Not a valid username");
        }
        else {
            messageBean.createNotificationMessage(MessageType.NOTIFICATION_SUCCESS, "Success!", "Password did match!");
            resetPassword(user, newPassword);
        }
    }

    /**
     *
     * Resets the users password and replaces it with a new
     * provided by the user.
     *
     * Promises that the newly provided password gets hashed
     * and stored in the database. After this change
     * provided RESET link-token will be deleted from the database
     * and should not be usable anymore.
     *
     *
     * @param user User or Admin for password change
     * @param newPassword newly provided password from User or Admin
     * @return true if user/admin got uppdated correctly. false
     * if otherwise.
     */
    public boolean resetPassword(User user, String newPassword) {
        boolean success;

        try {
            newPassword = PasswordHash.createHash(newPassword);
            user.setPassword(newPassword);
            userService.updateUser(user);
            settingService.removeSettingByGUID(resetPasswordToken);
            success = true;
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            success = false;
        }

        return success;
    }


    /* ----------------- */
    /* Setters & Getters */
    /* ----------------- */

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }

    public String getResetPasswordToken() {
        return resetPasswordToken;
    }

    public void setResetPasswordToken(String resetPasswordToken) {
        this.resetPasswordToken = resetPasswordToken;
    }
}
