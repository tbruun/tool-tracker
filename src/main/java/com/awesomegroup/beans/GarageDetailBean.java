package com.awesomegroup.beans;

import com.awesomegroup.model.Garage;
import com.awesomegroup.model.Tool;
import com.awesomegroup.service.GarageService;
import com.awesomegroup.service.ToolService;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Backing bean for the view of a single garage.
 */
@Named("garageDetailBean")
@SessionScoped
public class GarageDetailBean implements Serializable {

    /* ------------ */
    /* Dependencies */
    /* ------------ */

    @Inject
    private GarageService garageService;
    @Inject
    private ToolService toolService;
    @Inject
    private NavigationBean navigationBean;
    @Inject
    private SessionBean sessionBean;

    /* --------------- */
    /* Instance fields */
    /* --------------- */

    private Garage selectedGarage; //Garage instance that will be set when navigating to details page.
    private List<Tool> toolsInGarage; //List of the tools currently in the garage.

    /* ------- */
    /* Methods */
    /* ------- */

    /**
     * This method is called from .jsf-page and returns a string, which will change the view.
     * @param {@code Garage} The garage which this details view is based upon
     * @return {@code String} navigation
     */
    public String navigateToDetail(Garage garage) {

        selectedGarage = garage;
        updateToolList();
        return navigationBean.toGarageDetails();
    }

    /**
     * Allows logged in user to checkout a tool from the garage details page.
     */
    public void checkoutToolFromTable() {
        sessionBean.checkOutTool();
        updateToolList();
    }

    /**
     * Makes sure that the list of tools is properly updated.
     */
    public void updateToolList() {
        toolsInGarage = new ArrayList<>();
        selectedGarage = garageService.findGarageWithTools(selectedGarage.getId());
        toolsInGarage.addAll(selectedGarage.getTools()
                .stream()
                .filter(t -> t.getUser() == null && t.getStillAlive() == 1)
                .collect(Collectors.toList()));
    }

    /**
     * This deletes a {@code Garage}. If successful will navigate user back to {@link GarageBean}
     * @return {@code String} navigation to garage view
     */
    public String deleteGarage(){
        selectedGarage.setStillAlive(0);

        if (garageService.updateGarage(selectedGarage)) {
            return navigationBean.toGarage();
        }else{
            return null;
        }
    }

    /**
     * Used for changing the value of a {@code Garage}s  name
     * @param e event from editing a {@code Garage}.
     */
    public void handleNameSave(ValueChangeEvent e) {

        selectedGarage.setGarageName(e.getNewValue().toString());
        garageService.updateGarage(selectedGarage);
    }
    /**
     * Used for changing the value of a {@code Garage}s  location
     * @param e event from editing a {@code Garage}.
     */
    public void handleLocationSave(ValueChangeEvent e) {

        selectedGarage.setLocation(e.getNewValue().toString());
        garageService.updateGarage(selectedGarage);
    }
    /**
     * Used for changing the value of a {@code Garage}s  notes
     * @param e event from editing a {@code Garage}.
     */
    public void handleNotesSave(ValueChangeEvent e) {

        selectedGarage.setNotes(e.getNewValue().toString());
        garageService.updateGarage(selectedGarage);
    }

    /* ----------------- */
    /* Getters / Setters */
    /* ----------------- */

    public Garage getSelectedGarage() {
        return selectedGarage;
    }

    public void setSelectedGarage(Garage selectedGarage) {
        this.selectedGarage = selectedGarage;
    }

    public List<Tool> getToolsInGarage() {
        return toolsInGarage;
    }

    public void setToolsInGarage(List<Tool> toolsInGarage) {
        this.toolsInGarage = toolsInGarage;
    }
}
