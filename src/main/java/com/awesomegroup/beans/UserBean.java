package com.awesomegroup.beans;

import com.awesomegroup.model.User;
import com.awesomegroup.service.MailService;
import com.awesomegroup.service.SettingService;
import com.awesomegroup.service.UserService;
import com.awesomegroup.util.Guids;
import com.awesomegroup.util.Mails;
import com.awesomegroup.util.PasswordHash;
import com.awesomegroup.util.Settings;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

/**
 *
 *Bean for general user handling.
 *
 */
@Named("userBean")
@RequestScoped
public class UserBean implements Serializable{

    /* ------------ */
    /* Dependencies */
    /* ------------ */

    @Inject
    private NavigationBean navigationBean;
    @Inject
    private UserService userService;
    @Inject
    private SessionBean sessionBean;
    @Inject
    private MailService mailService;
    @Inject
    private SettingService settingService;

    /* --------------- */
    /* Instance fields */
    /* --------------- */

    private User newUser = new User();
    private List<User> userList;

    /* ------- */
    /* Methods */
    /* ------- */


    @PostConstruct
    private void init(){
        updateUserList();
        sessionBean.updateUserTools();
    }


    /**
     *
     * Creates a new user with password.
     *
     * Promises that the users password gets salted and hashed
     * correctly and then proceeds add user.
     *
     */
    public void createNewUser(){
        String newPassword;
        try{
            newPassword = PasswordHash.createHash(newUser.getPassword());
            newUser.setPassword(newPassword);
        }catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
        }

        if (userService.addUser(newUser)) {
            updateUserList();
            newUser = new User();
        }
    }

    /**
     *
     * Stores reset "session" in database and
     * sends out reset email to user.
     *
     * @param email
     */
    public void resetPassword(String email) {
        User user = userService.findUserByEmail(email);
        String guid = saveResetInformationForUser(user);
        mailService.sendResetPasswordMail(guid, user.getEmail(), Mails.RESET_MAIL_SUBJECT, Mails.RESET_MAIL_MESSAGE);
    }

    /**
     *
     * Handles the setup and storing of reset-session in database.
     *
     * @param user User that is associated with thiss current password-reset
     * @return GUID for specific reset-session.
     */
    public String saveResetInformationForUser(User user) {
        settingService.addSettingToUser(Settings.RESET_PASSWORD, Guids.makeGuid(), user);
        return settingService.getSettingForUser(Settings.RESET_PASSWORD, user).getValue();
    }

    private void updateUserList(){
        userList = userService.getAllUsers();
    }

    /* ----------------- */
    /* Getters / Setters */
    /* ----------------- */

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public User getNewUser() {
        return newUser;
    }

    public void setNewUser(User newUser) {
        this.newUser = newUser;
    }
}
