package com.awesomegroup.beans;

import com.awesomegroup.model.Garage;
import com.awesomegroup.service.GarageService;
import com.awesomegroup.util.MessageType;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;

/**
 * This backing bean handles methods and dependency injection needed by the garage page.
 */
@Named("garageBean")
@RequestScoped
public class GarageBean implements Serializable {

    /* ------------ */
    /* Dependencies */
    /* ------------ */

    @Inject
    private GarageService garageService;
    @Inject
    private MessageBean messageBean;

    /* --------------- */
    /* Instance fields */
    /* --------------- */

    private List<Garage> garageList; // List that backs the main garage datatable
    private Garage newGarage; // Empty new garage instance staged for creation

    /* ------- */
    /* Methods */
    /* ------- */

    /**
     * This method runs every time this bean is created. Since it is request scoped
     * that happens everytime that the user clickes the 'Garage' tab in the navbar.
     */
    @PostConstruct
    public void init() {
        newGarage = new Garage();
        updateGarageList();
    }

    /**
     * Get all garages from the database and store in the list that populates the .jsf-page
     */
    public void updateGarageList() {
        garageList = garageService.getAllGarages();
    }

    /**
     * Create a new {@code Garage} and persist it in the database.
     */
    public void createNewGarage() {

        Date currentDate = new Date(Calendar.getInstance().getTime().getTime());

        newGarage.setCreated(currentDate);

        if (garageService.addGarage(newGarage) != null) {

            messageBean.createNotificationMessage
                    (MessageType.NOTIFICATION_SUCCESS,
                            "Success!",
                            newGarage.getGarageName() + " successfully created!");
            updateGarageList();
        } else {
            messageBean.createNotificationMessage(
                    MessageType.NOTIFICATION_ERROR,
                    "Failure!",
                    "No garage created! Try again!"
            );
        }
    }

    /* ----------------- */
    /* Getters / Setters */
    /* ----------------- */

    public Garage getNewGarage() {
        return newGarage;
    }

    public void setNewGarage(Garage newGarage) {
        this.newGarage = newGarage;
    }

    public List<Garage> getGarageList() {
        return garageList;
    }

    public void setGarageList(List<Garage> garageList) {
        this.garageList = garageList;
    }
}
