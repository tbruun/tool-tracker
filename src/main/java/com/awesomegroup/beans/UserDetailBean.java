package com.awesomegroup.beans;

import com.awesomegroup.model.History;
import com.awesomegroup.model.Tool;
import com.awesomegroup.model.User;
import com.awesomegroup.service.HistoryService;
import com.awesomegroup.service.ToolService;
import com.awesomegroup.service.UserService;
import org.primefaces.extensions.model.timeline.TimelineEvent;
import org.primefaces.extensions.model.timeline.TimelineModel;

import javax.enterprise.context.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * Bean handling backend for UserDetail page.
 *
 */
@Named("userDetailBean")
@SessionScoped
public class UserDetailBean implements Serializable {

    /* ------------ */
    /* Dependencies */
    /* ------------ */

    @Inject
    private UserService userService;
    @Inject
    private NavigationBean navigationBean;
    @Inject
    private HistoryService historyService;
    @Inject
    private ToolService toolService;

    /* --------------- */
    /* Instance fields */
    /* --------------- */

    private User selectedUser;

    private TimelineModel historyModel;
    private List<History> historyList;

    /* ------- */
    /* Methods */
    /* ------- */


    /**
     *
     * Sets up a timeline for user-tool checkout
     * and navigates user to UserDetails page.
     *
     * Promises to provide user with history
     * showing when and for how long user have had
     * all his/her tool/tools.
     *
     * @param user Current selected user
     * @return User to Details page.
     */
    public String navigateToUserDetails(User user){

        // Set selected user, list of history and timeline model.
        selectedUser = user;
        historyList = new ArrayList<>();
        historyModel = new TimelineModel();

        // Fetch selected tools user history
        historyList = historyService.getHistoryListByUserId(selectedUser.getId());

        // get instance of calander for two different Calander, this
        // to differ checkout date from current date.
        Calendar calThen = Calendar.getInstance();
        Calendar calNow = Calendar.getInstance();


        /**
         *  Iterates through historylist and adds it to the timeline.
         * Checks if tool has been checked in to a garage, if thats
         * the case then get checked-in time and add it to the timeline.
         * if there is no checked-in date, extend timeline for tool by
         * calling currentTimeMillis.
         */
        historyList.forEach(h -> {
            Tool tool = toolService.findByToolID(h.getTool().getId());

            if (h.getCheckedIn() != null) {
                calNow.setTimeInMillis(h.getCheckedIn().getTime());
            } else {
                calNow.setTimeInMillis(System.currentTimeMillis());
            }

            calThen.setTimeInMillis(h.getCheckedOut().getTime());


            historyModel.add(new TimelineEvent(tool.getId() + " " + tool.getToolName(), calThen.getTime(), calNow.getTime()));

        });


        return navigationBean.toUserDetails();
    }

    /* -------------------------------- */
    /* Handlers for information editing */
    /* -------------------------------- */

    public void handleFirstNameSave(ValueChangeEvent e) {

        selectedUser.setFirstName(e.getNewValue().toString());
        userService.updateUser(selectedUser);
    }

    public void handleLastNameSave(ValueChangeEvent e) {

        selectedUser.setLastName(e.getNewValue().toString());
        userService.updateUser(selectedUser);
    }

    public void handleUsernameSave(ValueChangeEvent e) {

        selectedUser.setUserName(e.getNewValue().toString());
        userService.updateUser(selectedUser);
    }
    public void handleUserTypeSave(ValueChangeEvent e) {

        selectedUser.setUserType(e.getNewValue().toString());
        userService.updateUser(selectedUser);
    }
    public String deleteUser(){

        selectedUser.setTools(null);
        selectedUser.setStillAlive(0);

        if(userService.deleteUser(selectedUser)){
            return navigationBean.toUsers();
        }else{
            return null;
        }
    }

    /* ----------------- */
    /* Getters / Setters */
    /* ----------------- */

    public User getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(User selectedUser) {
        this.selectedUser = selectedUser;
    }

    public TimelineModel getHistoryModel() {
        return historyModel;
    }

    public void setHistoryModel(TimelineModel historyModel) {
        this.historyModel = historyModel;
    }

    public List<History> getHistoryList() {
        return historyList;
    }

    public void setHistoryList(List<History> historyList) {
        this.historyList = historyList;
    }
}
