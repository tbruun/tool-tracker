package com.awesomegroup.beans;

import com.awesomegroup.model.History;
import com.awesomegroup.model.Tool;
import com.awesomegroup.model.User;
import com.awesomegroup.service.HistoryService;
import com.awesomegroup.service.ToolService;
import com.awesomegroup.service.UserService;
import com.awesomegroup.util.MessageType;
import org.primefaces.extensions.model.timeline.TimelineEvent;
import org.primefaces.extensions.model.timeline.TimelineModel;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Backing bean for handling a specific {@code Tool}. Editing a {@code Tool}s attributes or deleting it.
 */
@Named("toolDetailBean")
@SessionScoped
public class ToolDetailBean implements Serializable {

    /* ------------ */
    /* Dependencies */
    /* ------------ */

    @Inject
    private ToolService toolService;
    @Inject
    private NavigationBean navigationBean;
    @Inject
    private HistoryService historyService;
    @Inject
    private UserService userService;
    @Inject
    private MessageBean messageBean;

    /* --------------- */
    /* Instance fields */
    /* --------------- */

    private Tool selectedTool;          //The tool which to delete, or edit.
    private TimelineModel historyModel; //The graph of activity for selected tool.
    private List<History> historyList;  //List of history for selected tool.

    /* ------- */
    /* Methods */
    /* ------- */

    /**
     * When logged in user selects a {@code Tool} to view in detail, he calls this method. Instance field selectedTool
     * will be set to param.
     *
     * @param tool is the selected {@code Tool} on which this view is based upon.
     * @return the navigational string to tooldetail.jsf.
     */
    public String navigateToDetail(Tool tool) {

        selectedTool = tool;
        historyList = new ArrayList<>();
        historyModel = new TimelineModel();

        // Fetch selected tools user history
        historyList = historyService.getHistoryListByToolId(selectedTool.getId());

        Calendar cal = Calendar.getInstance();


        historyList.forEach(h -> {
            User user = userService.findByUserID(h.getUser().getId());
            cal.setTimeInMillis(h.getCheckedOut().getTime());
            historyModel.add(new TimelineEvent(user.getFirstName() + " " + user.getLastName(), cal.getTime()));

        });
        return navigationBean.toToolDetails();
    }

    /**
     * Soft delete of a tool that takes logged in user to tools.jsf if successful. Else he stays on the same .jsf page.
     *
     * @return navigational string to tools.jsf, {@code null} for staying on the same page.
     */
    public String deleteTool() {
        selectedTool.setGarage(null);
        selectedTool.setUser(null);
        selectedTool.setStillAlive(0);

        if (toolService.updateTool(selectedTool)) {
            return navigationBean.toTools();
        } else {
            return null;
        }
    }

    /**
     * Used for changing the value of a {@code Tool}s  name
     *
     * @param e event from editing a {@code Tool}.
     */
    public void handleNameSave(ValueChangeEvent e) {

        if (e.getNewValue().toString().isEmpty()) {
            messageBean.createNotificationMessage(MessageType.NOTIFICATION_ERROR,
                    "Failed.",
                    "Name not changed, name is required.");
            selectedTool.setToolName(e.getOldValue().toString());


        } else {
            messageBean.createNotificationMessage(MessageType.NOTIFICATION_SUCCESS,
                    "Success!",
                    "Name changed to" + e.getNewValue().toString());
            toolService.updateTool(selectedTool);
            selectedTool.setToolName(e.getNewValue().toString());
        }

    }

    /**
     * Used for changing the value of a {@code Tool}s  description
     *
     * @param e event from editing a {@code Tool}.
     */
    public void handleDescriptionSave(ValueChangeEvent e) {

        messageBean.createNotificationMessage(MessageType.NOTIFICATION_SUCCESS,
                "Success!",
                "Description changed to" + e.getNewValue().toString());
        selectedTool.setDescription(e.getNewValue().toString());
        toolService.updateTool(selectedTool);
    }

    /**
     * Used for changing the value of a {@code Tool}s  notes
     *
     * @param e event from editing a {@code Tool}.
     */
    public void handleNotesSave(ValueChangeEvent e) {

        messageBean.createNotificationMessage(MessageType.NOTIFICATION_SUCCESS,
                "Success!",
                "Notes changed to" + e.getNewValue().toString());
        selectedTool.setNotes(e.getNewValue().toString());
        toolService.updateTool(selectedTool);
    }

    /* ----------------- */
    /* Getters / Setters */
    /* ----------------- */

    public Tool getSelectedTool() {
        return selectedTool;
    }

    public void setSelectedTool(Tool selectedTool) {
        this.selectedTool = selectedTool;
    }

    public TimelineModel getHistoryModel() {
        return historyModel;
    }

    public void setHistoryModel(TimelineModel historyModel) {
        this.historyModel = historyModel;
    }

    public List<History> getHistoryList() {
        return historyList;
    }

    public void setHistoryList(List<History> historyList) {
        this.historyList = historyList;
    }
}
