package com.awesomegroup.beans;

import com.awesomegroup.model.Category;
import com.awesomegroup.model.Garage;
import com.awesomegroup.model.History;
import com.awesomegroup.model.Tool;
import com.awesomegroup.service.GarageService;
import com.awesomegroup.service.HistoryService;
import com.awesomegroup.service.ToolService;
import com.awesomegroup.service.UserService;
import com.awesomegroup.util.MessageType;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Backing bean for the view of all tools. From this view the logged in user can view a table of all tools, and if he is
 * an admin he can also create a new {@code Tool}.
 */
@Named("toolBean")
@RequestScoped
public class ToolBean implements Serializable {

    /* ------------ */
    /* Dependencies */
    /* ------------ */

    @Inject
    ToolService toolService;
    @Inject
    UserService userService;
    @Inject
    HistoryService historyService;
    @Inject
    NavigationBean navigationBean;
    @Inject
    SessionBean sessionBean;
    @Inject
    GarageService garageService;
    @Inject
    MessageBean messageBean;

    /* --------------- */
    /* Instance fields */
    /* --------------- */

    public static final long serialVersionUID = 1L;
    private Tool newTool = new Tool();              //A new instance of a tool for persisting to database
    private List<Tool> tools;                       //List of all available tools
    private Map<Tool, Integer> activityScores;      //Map to read activity from
    private List<Category> categories;              //List of all categories from look up table in database

    /* ------- */
    /* Methods */
    /* ------- */

    /**
     * Runs automatically after the bean is created. Updates lists of tools, activityscores and categories.
     */
    @PostConstruct
    private void init() {
        updateToolList();
        updateActivityScores();
        categories = toolService.getCategories();
        sessionBean.updateUserTools();
    }

    /**
     * Creates a new {@code Tool} with the use of the instance field "newTool", whose values have been set from
     * the .JSF page.
     * @param garageId the primary key, ID, of the {@code Garage} in which the {@code Tool} is created.
     */
    public void createTool(String garageId) {

        System.out.println(newTool.getCategory());

        Garage garage = garageService.findGarageById(Integer.parseInt(garageId));
        newTool = toolService.addTool(newTool, garage);

        if (newTool == null) {
            messageBean.createPromptMessage(MessageType.PROMPT_ERROR, "Failed!", "An error has occurred. Failed to create tool.");
        } else {
            messageBean.createPromptMessage(MessageType.PROMPT_SUCCESS, "Success!", "Tool successfully created with ID:" + newTool.getId());
            updateToolList();
            updateActivityScores();

            List<String> cat = categories.stream().map(Category::getCategory).collect(Collectors.toList());
            if(!cat.contains(newTool.getCategory())){
                toolService.createCategory(newTool.getCategory());
            }
            newTool = new Tool();
        }
    }

    /**
     * Creates a score of activity based on a {@code Tool}.
     * @param tool the {@code Tool} to get the score of.
     * @return an int representing the activity of a {@code Tool}
     */
    public int getActivityScore(Tool tool) {
        if (activityScores.containsKey(tool)) {
            return activityScores.get(tool);
        } else {
            return 0;
        }
    }

    /**
     * Calls to this method is done when in the same bean to make sure that
     * the activity score is correctly updated and displayed.
     */
    public void updateActivityScores() {
        activityScores = new HashMap<>();
        Calendar threeMonthsAgo = Calendar.getInstance();
        threeMonthsAgo.add(Calendar.MONTH, -3);

        //Map number of history events that have taken place the last three months
        for (Tool tool : tools) {

            List<History> historyList = tool.getHistoryList().stream()
                    .filter((h) -> h.getCheckedOut().getTime() >= threeMonthsAgo.getTimeInMillis())
                    .collect(Collectors.toList());

            activityScores.put(tool, historyList.size());
        }

        int max = 0;

        //find max
        for (int i : activityScores.values()) {
            max = Math.max(i, max);
        }

        //set new values based on percentage of max value
        for (Map.Entry<Tool, Integer> entry : activityScores.entrySet()) {
            int oldValue = entry.getValue();
            entry.setValue((int) Math.round(oldValue * 100.0 / max));
        }
    }

    /**
     * Allows for a logged in user to check out a {@code Tool} from the list of {@code Tool}s in this view.
     */
    public void checkoutToolFromTable() {
        sessionBean.checkOutTool();
        updateToolList();
    }

    /**
     * Suggestions for {@code Category} when creating a new {@code Tool}.
     * @param query the {@code String} from the input field.
     * @return a list of {@String}s matching the query
     */
    public List<String> completeText(String query) {

        List<Category> categories = toolService.getCategories();

        return categories.stream()
                .filter(category -> category.getCategory().toUpperCase().startsWith(query.toUpperCase()))
                .map(Category::getCategory)
                .collect(Collectors.toList());
    }


    /* ----------------- */
    /* Getters / Setters */
    /* ----------------- */


    public Tool getNewTool() {
        return newTool;
    }

    public void setNewTool(Tool newTool) {
        this.newTool = newTool;
    }

    public void updateToolList() {
        tools = toolService.findAllDeep();
    }

    public List<Tool> getTools() {
        return tools;
    }

    public void setTools(List<Tool> tools) {
        this.tools = tools;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }
}
