package com.awesomegroup.service;

import com.awesomegroup.model.Garage;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.util.List;

/**
 * Service that handles all requests concerning {@code Garage}s. There are methods for retrieving entities from database
 * using the models, {@code Garage}, named queries where available.
 */
@Stateless
public class GarageService implements Serializable {

    @PersistenceContext(unitName = "primary")
    protected EntityManager em;

    /**
     * Queries the database for all {@code Garage}s.
     * @return a list of all {@code Garage}s.
     */
    public List<Garage> getAllGarages(){
        return em.createNamedQuery("Garage.findAll", Garage.class).getResultList();
    }

    /**
     * Persists a {@code Garage} entity to the database.
     * @param garage the {@code Garage} to persist.
     * @return the saved {@code Garage}.
     */
    public Garage addGarage(Garage garage){
        em.persist(garage);
        return garage;
    }

    /**
     * Queries the database for a {@code Garage} given a user input.
     * @param garageName  {@code String} which to search the database for.
     * @return the found entity.
     */
    public Garage findGarageByName(String garageName){

        return em.createNamedQuery("Garage.findByUserName", Garage.class)
                .setParameter("garageName", garageName)
                .getSingleResult();
    }

    public Garage findGarageById(int id){
        return em.find(Garage.class, id);
    }

    /**
     * Retrieves a {@code Garage} with managed {@code Tool}s, using Entity Graph.
     * @param id the primary key of the entity to find.
     * @return the found entity.
     */
    public Garage findGarageWithTools(int id){
        Garage garage;
        try {
            garage = em.createNamedQuery("Garage.findGarageById", Garage.class)
                    .setParameter("id", id)
                    .setHint("javax.persistence.fetchgraph", em.getEntityGraph("FetchGarageWithTool"))
                    .getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
        return garage;
    }

    /**
     * Persist the entity with updated attributes.
     * @param garage is the entity to persist
     * @return {@code true} or {@code false} depending on merge success.
     */
    public boolean updateGarage(Garage garage) {

        garage = em.merge(garage);
        return garage != null;
    }
}
