package com.awesomegroup.service;

import com.awesomegroup.model.History;
import com.awesomegroup.model.Tool;
import com.awesomegroup.model.User;
import com.awesomegroup.util.Mails;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.List;
import java.util.Properties;

/**
 * This is the service that allows email-handling.<p>
 *
 * There is not much going into sending emails from this software: <br>
 *
 * By calling {@code sendMail(String recipient, String subject, String msg)} you send a html formatted email given
 * your parameters.<p>
 *
 * This class is still a work-in-progress, so it's likely to change in the near future.
 */
@Stateless
public class MailService {

    private final String username;
    private final String password;
    Session session;
    Properties props;

    @Inject
    HistoryService historyService;
    @Inject
    UserService userService;
    @Inject
    SettingService settingService;

    /**
     *  Might want to change this to a @PostConstruct someday.
     *  Sets up necessary properties for email handling.
     */
    public MailService() {
        password = "guide2JSON";
        username = "lolautobuilder@gmail.com";

        props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
    }

    /**
     * Send a html formatted email based on your parameters.
     *
     * @param recipient {@code String} email address for recipient.
     * @param subject {@code String} subject row for the email.
     * @param msg {@code String} html formatted email body.
     */
    public void sendMail(String recipient, String subject, String msg) {

        try {
            Message message = new MimeMessage(session);
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(recipient));
            message.setSubject(subject);
            message.setContent(msg, "text/html; charset=utf-8");

            Transport.send(message);

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Sends an email with instructions to user for how to reset his password.
     *
     * @param guid {@code String} user GUID for token link.
     * @param recipient {@code String} subject row for the email.
     * @param subject {@link Mails} subject row for the email.
     * @param msg {@link Mails} html formatted email body.
     */
    public void sendResetPasswordMail(String guid, String recipient, Mails subject, Mails msg) {
        sendMail(recipient, subject.getValue(), msg.getValue() + "<a href="+"\"" + "http://localhost:8080/resetpassword.jsf?token="+ guid + "\">RESET</a>");
    }


    /**
     * Generates the email message body sent to admins with a list of old tools
     *
     * @param oldTools tools to be included in the mail
     * @return message to send in the email.
     */
    public String generateOldToolMessage(List<Tool> oldTools) {
        StringBuilder message = new StringBuilder();

        message.append("<h4>The following tools have been with the same user for 3 months:</h4>");

        oldTools.stream().forEach(tool -> {
            List<History> historyList = tool.getHistoryList();
            History latest = historyList.get(historyList.size() - 1);
            User user = tool.getUser();

            message.append("<p>")
                    .append("<strong>Tool:</strong> ").append(tool.getToolName()).append(" (").append(tool.getId()).append(")")
                    .append("<br/>")
                    .append("<strong>Current user:</strong> ").append(user.getFirstName()).append(" ").append(user.getLastName())
                    .append("<br/>")
                    .append("<strong>Taken over:</strong> ").append(latest.getCheckedOut())
                    .append("</p>");
        });
        return message.toString();
    }
}
