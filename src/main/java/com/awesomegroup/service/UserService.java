package com.awesomegroup.service;

import com.awesomegroup.model.User;
import com.awesomegroup.util.PasswordHash;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

/**
 * Service for handling requests concerning {@code User} entities. Methods for validating {@code User}-credentials
 * and for retrieving entities from database.
 */
@Stateless
public class UserService implements Serializable {

    @PersistenceContext(unitName = "primary")
    protected EntityManager em;

    /**
     * Validates a login-attempt from a user. Uses utility-class {@link PasswordHash} to validate password.
     * @param userName is the username from login-form.
     * @param password is the password from login-form.
     * @return the {@code User} if login is correct. Else {@code null}.
     */
    public User userLogin(String userName, String password) {
        User user = findByBinaryUsername(userName);

        try {
            if (PasswordHash.validatePassword(password, user.getPassword())) {
                return user;
            }
        } catch (NoSuchAlgorithmException | InvalidKeySpecException | NullPointerException e) {

        }

        return null;
    }

    /**
     * Queries the database for a {@code User} given a username.
     * @param username the {@code String} for which to search.
     * @return the {@code User} found by query. Else {@code null} if no entity found.
     */
    public User findByUsername(String username) {
        User user;
        try {
            user = em.createNamedQuery("User.findByUserName", User.class)
                    .setParameter("userName", username)
                    .getSingleResult();
        } catch (NoResultException ex) {
            user = null;
        }
        return user;
    }

    /**
     * Queries the database for a {@code User} given a username, this query is case-sensitive.
     * @param username the {@code String} for which to search.
     * @return the {@code User} found by query. Else {@code null} if no entity found.
     */
    public User findByBinaryUsername(String username) {
        User user;
        try {
            user = em.createNamedQuery("User.findByBinaryUsername", User.class)
                    .setParameter("userName", username)
                    .getSingleResult();
        } catch (NoResultException ex) {
            user = null;
        }
        System.out.println("USERNAME:" + username);
        return user;
    }

    /**
     * Queries the database for a {@code User} given an email.
     * @param email is the unique email with which to search.
     * @return the {@code User} found by query. Else {@code null} if no entity found.
     */
    public User findUserByEmail(String email) {
        User user;
        try {
           user = em.createQuery("SELECT u FROM User u WHERE u.email =:email", User.class)
                    .setParameter("email", email)
                    .getSingleResult();
        } catch (NoResultException e) {
            user = null;
        }
         return user;
    }

    /**
     * Queries database for all {@code User}s with user type 'admin'.
     * @return a list of all {@code User}s tagged as admin.
     */
    public List<User> findAdmins() {
        return em.createQuery("SELECT u FROM User u WHERE u.userType = :userType", User.class)
                .setParameter("userType", "admin")
                .getResultList();
    }

    /**
     * Queries the database for a {@code User} given an ID.
     * @param userID is the unique id with which to search.
     * @return the {@code User} found by query. Else {@code null} if no entity found.
     */
    public User findByUserID(int userID) {
        User user;
        try {
            user = em.find(User.class, userID);
        } catch (NoResultException ex) {
            user = null;
        }
        return user;
    }

    /**
     * Queries the database for all {@code User}s.
     * @return list of {@code User}s
     */
    public List<User> getAllUsers(){

        List<User> userList;
        try {
            userList = em.createNamedQuery("User.findAll", User.class).getResultList();
        } catch (NoResultException ex) {
            userList = null;
        }
        return userList;
    }

    /**
     * Remove {@code User} with given primary key, ID.
     * @param userID the primary key of the user to delete.
     */
    public void removeUser(int userID) {
        //might want to make this a boolean and run em.find before removing...
        User toRemove = em.find(User.class, userID);
        em.remove(toRemove);
    }

    /**
     * Persist {@code User} to the database
     * @param user is the {@code User} to persist.
     * @return {@code true} or {@code false}
     */
    public boolean addUser(User user) {

        try {
            em.persist(user);
        } catch (PersistenceException e) {
            user = null;
        }
        return user != null;
    }
    /**
     * Update {@code User} in the database
     * @param user is the {@code User} to update.
     * @return {@code true} or {@code false}
     */
    public boolean updateUser(User user) {

        user = em.merge(user);

        return user != null;
    }

    /**
     * Delete {@code User} to from database. Is a soft delete.
     * Changing stillAlive to 0.
     * @param user is the {@code User} to persist.
     * @return {@code true} or {@code false}
     */
    public boolean deleteUser(User user){
        user.setStillAlive(0);
        user.setTools(null);
        em.merge(user);

        return user.getStillAlive() != 1;
    }

}