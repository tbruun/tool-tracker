package com.awesomegroup.service;

import com.awesomegroup.model.History;
import com.awesomegroup.model.Tool;
import com.awesomegroup.model.User;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * This is the service that handles History.<p>
 * For description about a History-event read the {@link History} documentation.
 */
@Stateless
public class HistoryService implements Serializable {

    @PersistenceContext
    EntityManager em;

    /**
     * Gives a list of tools that have been associated with a user for more than 3 months.
     * @return {@code List<Tool>} with inactive tools.
     */
    public List<Tool> findOldTools() {
        List<History> historyList = em.createNamedQuery("History.findOldTools", History.class).getResultList();
        List<Tool> oldTools = new ArrayList<>();
        historyList.forEach(history -> oldTools.add(history.getTool()));
        return oldTools;
    }

    /**
     * Creates a history event when a user takes a tool from database,
     * and closes the previous history-event if the tool is taken from another user. <br>
     *
     * This method should be called whenever a tool is taken over from a user.
     *
     * @param tool {@link Tool} we want to update
     * @param user {@link User} who takes over the tool
     * @return true if history update was successful, else false.
     */
    public boolean updateHistory(Tool tool, User user) {
        boolean success = false;
        Timestamp now = new Timestamp(Calendar.getInstance().getTime().getTime());
        History last = getLatestHistoryByTool(tool.getId());

        if(last != null) {
            last.setCheckedIn(now);
            em.merge(last);
        }

        History newEntry = new History();
        newEntry.setCheckedOut(now);
        newEntry.setTool(tool);
        newEntry.setUser(user);
        em.persist(newEntry);

        if (em.find(History.class, newEntry.getId()) != null) {
            success = true;
        }

        return success;
    }

    /**
     * Updates a history event when a user delivers a {@link Tool} back to garage.
     * Closes the history-event by adding a checkedIn timestamp.  <br>
     *
     * This method should be called whenever a tool is returned to a garage.
     *
     * @param tool {@link Tool} we want to update
     * @return true if history update was successful, else false.
     */
    public boolean updateHistory(Tool tool) {
        Timestamp now = new Timestamp(Calendar.getInstance().getTime().getTime());

        History last = getLatestHistoryByTool(tool.getId());

        if(last != null) {
            last.setCheckedIn(now);
            em.merge(last);
            return true;
        }

        return false;
    }

    /**
     * gives the latest history event associated with a tool id.
     * @param id id of tool.
     * @return latest {@link History} event associated with the tool id
     */
    public History getLatestHistoryByTool(int id) {
        try {
            return em.createQuery("SELECT h FROM History h WHERE h.tool.id = :tool_id order by h.checkedOut desc", History.class)
                    .setMaxResults(1)
                    .setParameter("tool_id", id)
                    .getResultList().get(0);
        } catch (NoResultException | IndexOutOfBoundsException e) {
            return null;
        }
    }

    /**
     * gives a list of history based on tool id.
     * This is equivalent with {@code tool.getHistoryList()}, but this allows you to get the list without first having
     * to look up the tool, thus reducing requests to the database.
     * @param id {@code int} tool id
     * @return {@code List<History>} for history elements by tool id.
     */
    public List<History> getHistoryListByToolId(int id) {
        return em.createQuery("SELECT h FROM History h WHERE h.tool.id = :tool_id", History.class)
                .setParameter("tool_id", id)
                .getResultList();
    }

    public List<History> getHistoryListByUserId(int id) {
        return em.createQuery("SELECT h FROM History h WHERE h.user.id = :user_id", History.class)
                .setParameter("user_id", id)
                .getResultList();
    }
}
