package com.awesomegroup.service;

import com.awesomegroup.model.Setting;
import com.awesomegroup.model.User;
import com.awesomegroup.util.Settings;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This is the service that allows Settings-handling.<p>
 *
 * In order to handle settings for users this class there is a few convenience methods available in this class.<p>
 *
 *     - To add a setting to a user call {@code addSettingToUser(Settings key, String value, User user)} <br>
 *     - To read a setting from a user call {@code getSettingForUser(Settings key, User user)}<br>
 *     - To update a setting for a user call {@code updateSetting(String value, Setting setting)}
 */
@Stateless
public class SettingService implements Serializable {

    @PersistenceContext(unitName = "primary")
    protected EntityManager em;

    /**
     * Retrieves the setting for a User based on Setting key.<br>
     *
     * @param key Enum {@link Settings} key for the setting.
     * @param user user the {@code Setting} is associated with.
     * @return requested setting if it exist or {@code null} if it could not be found.
     */
    public Setting getSettingForUser(Settings key, User user) {
        user = em.find(User.class, user.getId());
        try {
            return user.getSettings()
                    .stream()
                    .filter(setting -> setting.getKey().equals(key))
                    .collect(Collectors.toList())
                    .get(0);
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    /**
     * Adds a Setting to a user and persists it in the setting table.
     *
     * @param key Enum {@link Settings} key for the setting.
     * @param value {@String} value for the setting.
     * @param user user the {@code Setting} is associated with.
     * @return true if setting was successfully added. false if something went wrong.
     */
    public boolean addSettingToUser(Settings key, String value, User user) {
        User u;
        Setting s;
        try {
            s = new Setting(key,value);
            s = em.merge(s);
            u = em.merge(user);
            s.setUser(u);
        } catch (PersistenceException e) {
            s = null;
        }
        return s != null;
    }

    /**
     * Updates a current setting in the setting table.
     *
     * @param value {@code String} value to set to the setting.
     * @param setting {@link Setting} the Setting you want to update.
     * @return true if setting was successfully changed. false if something went wrong.
     */
    public boolean updateSetting(String value, Setting setting) {
        Setting s;
        try {
            s = em.find(Setting.class, setting.getId());
            s.setValue(value);
            s = em.merge(s);
        } catch (PersistenceException e) {
            s = null;
        }
        return s != null;
    }

    /**
     * Removes all settings for a given key.<br>
     *     WARNING: This method has very few real world uses, and is potentially dangerous to the settings table.<br>
     *     This is mostly used as a temporary hack for nightly removal of RESET_MAIL guids.
     *
     * @param settingKey the key you want to remove ALL settings of.
     */
    public void removeAllSettingsByKey(Settings settingKey) {

        List<Setting> settingsList = em.createQuery("SELECT s FROM Setting s WHERE s.key =:settingKey", Setting.class)
                .setParameter("settingKey", settingKey)
                .getResultList();

        for(Setting s : settingsList) {
            em.remove(s);
        }
    }

    /**
     * Removes a setting given a GUID.
     *
     * @param guid {@code String} GUID to remove from database.
     * @return true if setting was removed, or false if no such setting was found.
     */
    public boolean removeSettingByGUID(String guid) {
        Setting setting;
        try {
            setting = em.createQuery("SELECT s FROM Setting s WHERE s.value =:guid", Setting.class)
                    .setParameter("guid", guid)
                    .getSingleResult();
            em.remove(setting);
            return true;
        } catch (NoResultException e) {
            return false;
        }
    }

    /**
     * Finds a user from his GUID {@code String}.
     *
     * @param guid {@code String} GUID to find from Settings.
     * @return User by the GUID String, or {@code null} if no such setting was found.
     */
    public User findUserBySettingGuid(String guid) {
        Setting setting;
        try {
            setting = em.createQuery("SELECT s FROM Setting s WHERE s.value =:guid", Setting.class)
                    .setParameter("guid", guid)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
        return setting.getUser();
    }
}