package com.awesomegroup.converter;

import com.awesomegroup.model.Garage;
import com.awesomegroup.service.GarageService;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

/**
 * Converter object for {@link Garage} data-type in jsf-objects.
 */
@FacesConverter("com.awesomegroup.converter.GarageConverter")
public class GarageConverter implements Converter {

    @Inject
    GarageService garageService;

    /**
     * {@inheritDoc}
     */
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return garageService.findGarageByName(value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        Garage g = (Garage) o;
        return g.getGarageName();
    }
}
