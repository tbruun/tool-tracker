package com.awesomegroup.converter;

import com.awesomegroup.model.Tool;
import com.awesomegroup.service.ToolService;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

/**
 * Converter object for {@link Tool} data-type in jsf-objects.
 */
@FacesConverter("com.awesomegroup.converter.ToolConverter")
public class ToolConverter implements Converter {

    @Inject
    ToolService toolService;

    /**
     * {@inheritDoc}
     */
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return toolService.findByToolName(value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        Tool t = (Tool) o;
        return t.getToolName();
    }
}
