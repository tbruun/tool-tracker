package com.awesomegroup.service;

import java.sql.Date;
import java.util.Calendar;

/**
 * Created by tbruun on 5/14/15.
 */
public class SampleData {

    //User candidate 1
    static final String USER1_FIRST_NAME = "Alfred";
    static final String USER1_LAST_NAME = "Hitchcock";
    static final String USER1_EMAIL = "test@test.com";
    static final String USER1_USERNAME = "freddan";
    static final String USER1_USER_TYPE = "user";
    static final String USER1_PASSWORD = "rearWindow";

    //User candidate 2
    static final String USER2_FIRST_NAME = "George W";
    static final String USER2_LAST_NAME = "Bush";
    static final String USER2_EMAIL = "test2@test2.com";
    static final String USER2_USERNAME = "bushan";
    static final String USER2_USER_TYPE = "admin";
    static final String USER2_PASSWORD = "obama";

    //Garage candidate
    static final String GARAGE_NAME = "Bygget på Ågatan";
    static final String GARAGE_LOCATION = "Ågatanvägen 2";
    static Date GARAGE_TIME_CREATED = new Date(Calendar.getInstance().getTime().getTime());


    //Tool candidate
    static final String TOOL_NAME = "Hilti ME-487 SE3";
    static final String TOOL_DESC = "Bra borrmaskin";
    static final String TOOL_CAT = "Borrmaskiner";
    static final String TOOL_NOTES = "ganska sliten";
}
